﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;

namespace MoveShapeDemo.Core
{
	public class MoveShapeHub : Hub
	{
		public void UpdateModel(ShapeModel clientModel) 
		{
			clientModel.LastUpdatedBy = Context.ConnectionId;
			Clients.AllExcept(clientModel.LastUpdatedBy).updateShape(clientModel);
		}
	}
}