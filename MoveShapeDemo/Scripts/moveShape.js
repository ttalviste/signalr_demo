var ShapeModel = (function () {
    function ShapeModel(left, top) {
        this.left = left;
        this.top = top;
    }
    return ShapeModel;
})();
var moveshape_demo;
(function (moveshape_demo) {
    var moveShapeHub = $.connection.moveShapeHub;
    var $shape = $("#shape");
    var messageFrequency = 10;
    var updateRate = 1000 / messageFrequency;
    var shapeModel = new ShapeModel(0, 0);
    var moved = false;
    moveShapeHub.client.updateShape = function (model) {
        shapeModel = model;
        $shape.css({
            left: model.left,
            top: model.top
        });
    };
})(moveshape_demo || (moveshape_demo = {}));
//@ sourceMappingURL=moveShape.js.map
