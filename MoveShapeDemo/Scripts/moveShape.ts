class ShapeModel
{
	constructor(public left : number, public top : number) { };
}

module moveshape_demo {
	declare var $;

	var moveShapeHub = $.connection.moveShapeHub;
	var $shape = $("#shape");
	var messageFrequency: number = 10;
	var updateRate = 1000 / messageFrequency;
	var shapeModel = new ShapeModel(0, 0);
	var moved: bool = false;

	moveShapeHub.client.updateShape = function (model: ShapeModel) {
		shapeModel = model;
		$shape.css({ left: model.left, top: model.top });
	};
	$.connec

}